# acme.sh

The purpose of this is to set up DNS-01 TLS validation for support of wildcard domains with Let's Encrypt and DuckDNS.
This is untested and unfinished!

A better alternative is probably to use certbot with [certbot_dns_duckdns](https://github.com/infinityofspace/certbot_dns_duckdns).
