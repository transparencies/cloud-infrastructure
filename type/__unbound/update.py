#!/usr/bin/env python3

import urllib.request, re, pathlib


def update_ads():
    src_url = "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling/hosts"

    folder = pathlib.Path("files")
    folder.mkdir(exist_ok=True)
    filename = folder / "blocklist.conf"

    print(f"Updating {filename}...")

    raw = urllib.request.urlopen(src_url).read().decode()

    matches = re.findall(r"^0\.0\.0\.0 ([^ ]*?)\s", raw, re.MULTILINE)

    with open(filename, "w") as f:
        f.write("server:\n")

        for a in matches:
            if a != "0.0.0.0":
                f.write(f'    local-zone: "{a}" always_refuse\n')


if __name__ == "__main__":
    update_ads()
